vlib work
vmap work work
###Design Files
vcom -2008 +cover=bcesft -work work  \
"../../rtl/uart/baud_gen.vhd" \
"../../rtl/command_handler/cmd_handler.vhd" \
"../../rtl/dac_controller/en_gen.vhd" \
"../../rtl/dac_controller/sclk_gen.vhd" \
"../../rtl/waveform_generator/sine_bram.vhd" \
"../../rtl/dac_controller/sync_data_gen.vhd" \
"../../rtl/uart/u_rx.vhd" \
"../../rtl/uart/u_tx.vhd" \
"../../rtl/uart/uart_pack.vhd" \
"../../rtl/uart/uart_top.vhd" \
"../../rtl/waveform_generator/wave_gen.vhd" \
"../../rtl/waveform_generator/wave_gen_pgk.vhd" \
"../../rtl/waveform_generator/wave_gen_top.vhd"