`include "uart_interface.sv"
`include "spi_interface.sv"
`include "tests.sv"

module testbench;

    logic clock = 0;
    logic reset = 1;

    always #5ns clock = ~clock;

    initial begin
        reset = 1;
        #10ns;
        reset = 0;
    end

    uart_interface uart_interface0(clock,reset);
    spi_interface spi_interface0(clock, reset);

    test test0(uart_interface0,spi_interface0);

        wave_gen_top #(
         .osc_freq(100_000_000), 
         .Data_width(8),
         .no_of_sample(16)
        )
        DUT ( 
        .Clock(clock),
        .reset(reset),
        .uart_din(uart_interface0.uart_tx),
        .sw(uart_interface0.sw),
        .uart_dout(uart_interface0.uart_rx),
        .sync_n(spi_interface0.sync_n),
        .dac_data(spi_interface0.dac_data),
        .sclk(spi_interface0.sclk),
        .led(spi_interface0.led)
        );

endmodule