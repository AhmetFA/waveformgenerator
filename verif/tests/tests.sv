`include "environment.sv"
program test(uart_interface uart_interface0, spi_interface spi_interface0);

    environment environment0;
    logic [7:0] tx_data[];
    logic [1:0] sw;

    initial begin
        environment0 = new(uart_interface0,spi_interface0);
        fork
            environment0.main();
        join_none
        
        $display("Test BEGINS here Now!");
        #1ms;
        sw = 0;
        tx_data = new[1];
        tx_data = '{8'h61};
        environment0.uart_agent0.uart_sequencer0.uart_data_write(tx_data,sw);
        #25ms;
        sw = 0;
        tx_data = new[1];
        tx_data = '{8'h62};
        environment0.uart_agent0.uart_sequencer0.uart_data_write(tx_data,sw);
        #25ms;
        for ( byte wave = 8'h41; wave < 8'h45; wave++)
            for( byte frq = 8'h30; frq <8'h40; frq++) begin
                tx_data = new[2];
                tx_data = {wave, frq};
                environment0.uart_agent0.uart_sequencer0.uart_data_write(tx_data,sw);
                #1ms;
            end
        
        $display("Test ENDS here Now!");
        $finish;
    end
endprogram