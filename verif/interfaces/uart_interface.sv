interface uart_interface(
    input  logic clock,
    input  logic reset
    );

    logic [2:0] sw;
    logic uart_tx;
    logic uart_rx;

    modport driver(
        output sw,
        input  uart_rx,
        output uart_tx
    );
    modport monitor(
        input sw,
        input uart_rx,
        input uart_tx
    );

    property simultaneus_transaction;
        @(posedge clock) !uart_tx |-> uart_rx;
    endproperty

    simultaneus_transaction_check : assert property (simultaneus_transaction);

endinterface //uart_interface()