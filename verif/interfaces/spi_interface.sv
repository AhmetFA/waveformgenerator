interface spi_interface(
    input  logic clock,
    input  logic reset
    );

    logic sync_n;
    logic dac_data;
    logic sclk;
    logic [7:0] led;

    modport monitor(
        input sync_n,
        input dac_data,
        input sclk,
        input led
    );

    property sclk_per;
        @(posedge clock)
        disable iff(reset) $rose(sclk) |-> ##2 !sclk;
    endproperty

    sclk_per_check : assert property (sclk_per);

    property sync_n_len;
        @(posedge sclk)
        disable iff(reset) $fell(sync_n) |-> ##16 sync_n;
    endproperty

    sync_n_len_check : assert property(sync_n_len);

endinterface //spi_interface()