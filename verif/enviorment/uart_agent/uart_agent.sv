`include "uart_driver.sv"
`include "uart_monitor.sv"
`include "uart_sequencer.sv"
class uart_agent;

    uart_driver uart_driver0;
    uart_monitor uart_monitor0;
    uart_sequencer uart_sequencer0;

    mailbox uart_monitor2scoreboard;
    mailbox uart_sequencer2driver;
    event stim_end;

    virtual uart_interface uart_vif;

    function new(virtual uart_interface uart_vif, mailbox uart_monitor2scoreboard);
        this.uart_vif = uart_vif;

        uart_sequencer2driver = new(1);

        uart_driver0 = new(uart_vif, uart_sequencer2driver, stim_end);
        uart_monitor0 = new(uart_vif, uart_monitor2scoreboard);
        uart_sequencer0 = new(uart_sequencer2driver, stim_end);
    endfunction

    task main();
        $display("Uart Agent started time: %0t", $time);
        fork
            uart_driver0.main();
            uart_monitor0.main();
        join
        $display("Uart Agent ended time: %0t", $time);
    endtask

endclass

