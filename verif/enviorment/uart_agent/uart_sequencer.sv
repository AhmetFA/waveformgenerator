class uart_sequencer;
    uart_seq_item uart_seq_item0;

    mailbox uart_sequencer2driver;
    event stim_end;

    function new(mailbox uart_sequencer2driver, event stim_end);
        this.uart_sequencer2driver = uart_sequencer2driver;
        this.stim_end = stim_end;
    endfunction

    task uart_data_write(logic [7:0] tx_data[], logic [1:0] sw);
        $display("/***UART DATA WRITE SEQUENCE CALLED***/");
        $display("time is : %0t, tx_data is : %p, sw is: %b",$time, tx_data, sw);
        foreach(tx_data[i]) begin
            uart_seq_item0 = new();
            uart_seq_item0.sw = sw;
            uart_seq_item0.tx_data = tx_data[i];
            uart_sequencer2driver.put(uart_seq_item0);
        end
        @(stim_end);
    endtask
endclass