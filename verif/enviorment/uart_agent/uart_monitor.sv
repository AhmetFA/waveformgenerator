class uart_monitor;

    //creating virtual interface handle
    virtual uart_interface uart_vif;
    mailbox uart_monitor2scoreboard;
    time uart_baud=104.167us;
    uart_seq_item uart_seq_item0;

    function new(virtual uart_interface uart_vif, mailbox uart_monitor2scoreboard);
        this.uart_vif = uart_vif;
        this.uart_monitor2scoreboard = uart_monitor2scoreboard;
    endfunction

    function uart_baud_decoder;
        if      (uart_vif.monitor.sw == 0)
            uart_baud = 104.167us;
        else if (uart_vif.monitor.sw == 1)
            uart_baud = 52.083us;
        else if (uart_vif.monitor.sw == 2)
            uart_baud = 26.042us;
        else if (uart_vif.monitor.sw == 3)
            uart_baud = 17.361us;
        else if (uart_vif.monitor.sw == 4)
            uart_baud = 8.681us;
        else 
            uart_baud = 104.167us;
    endfunction

    task main;
        $display("Uart Monitor started time: %0t", $time);
        forever begin
            uart_seq_item0 = new();
            fork
                begin
                    @(negedge uart_vif.monitor.uart_rx);
                    $display("receive detected");
                    uart_seq_item0.rx_captured = 1;
                    uart_baud_decoder();
                    uart_seq_item0.sw = uart_vif.monitor.sw;
                    #(uart_baud/2);
                    for(int i; i <8; i++) begin
                        #uart_baud;
                        uart_seq_item0.rx_data[i] = uart_vif.monitor.uart_rx;
                    end
                end
                begin
                    @(negedge uart_vif.monitor.uart_tx);
                    $display("transmit detected");
                    uart_seq_item0.tx_captured = 1;
                    uart_baud_decoder();
                    uart_seq_item0.sw = uart_vif.monitor.sw;
                    for(int i; i <8; i++) begin
                        #uart_baud;
                        uart_seq_item0.tx_data[i] = uart_vif.monitor.uart_tx;
                    end
                end
            join_any
            uart_monitor2scoreboard.put(uart_seq_item0);
        end
        $display("Uart Monitor ended time: %0t", $time);
    endtask
endclass