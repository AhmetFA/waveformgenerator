class uart_seq_item;

    logic [7:0]tx_data =0;
    logic [7:0]rx_data =0;
    logic [1:0] sw=0;
    //monitor bits
    bit tx_captured=0;
    bit rx_captured=0;
    function void display();
    $display("-------------------------");
    $display("tx_data : %h",tx_data);
    $display("rx_data : %h",rx_data);
    $display("sw : %h",sw);
    $display("tx_captured: %h",tx_captured);
    $display("rx_captured: %h",rx_captured);
    $display("-------------------------");
  endfunction
endclass