`include "uart_seq_item.sv"
class uart_driver;

    virtual uart_interface uart_vif;

    mailbox uart_sequencer2driver;

    time uart_baud=104.167us;
    event stim_end;
    

    function new(virtual uart_interface uart_vif, mailbox uart_sequencer2driver, event stim_end);

        this.stim_end = stim_end;

        this.uart_vif = uart_vif;

        this.uart_sequencer2driver = uart_sequencer2driver;

    endfunction

    task main;
        $display("Uart Driver started time: %0t", $time);
        uart_vif.driver.sw= 0;
        uart_vif.driver.uart_tx = 1;
        forever begin
            uart_seq_item uart_seq_item0;
            $display("Uart Driver waiting for item %t", $time);
            uart_sequencer2driver.get(uart_seq_item0);
            $display("/***Driver is  CALLED***/");
            $display("time is : %0t, tx_data is : %p, sw is: %b",$time, uart_seq_item0.tx_data, uart_seq_item0.sw);
            @(posedge uart_vif.clock);
            uart_vif.driver.sw = uart_seq_item0.sw;
            if      (uart_seq_item0.sw == 0)
                uart_baud = 104.167us;
            else if (uart_seq_item0.sw == 1)
                uart_baud = 52.083us;
            else if (uart_seq_item0.sw == 2)
                uart_baud = 26.042us;
            else if (uart_seq_item0.sw == 3)
                uart_baud = 17.361us;
            else if (uart_seq_item0.sw == 4)
                uart_baud = 8.681us;
            else 
                uart_baud = 104.167us;
            uart_vif.driver.uart_tx = 0;
            #uart_baud;
            for (int i = 0; i < 8; i++) begin
                uart_vif.driver.uart_tx = uart_seq_item0.tx_data[i];
                #uart_baud;
            end
            uart_vif.driver.uart_tx = 1;
            #uart_baud;
            -> stim_end;
        end
        $display("Uart Driver ended time: %0t", $time);
    endtask

endclass