`include "uart_agent.sv"
`include "spi_monitor.sv"
`include "scoreboard.sv"
class environment;

    virtual uart_interface uart_vif;
    virtual spi_interface spi_vif;

    scoreboard scoreboard0;
    uart_agent uart_agent0;
    spi_monitor spi_monitor0;

    mailbox uart_monitor2scoreboard;
    mailbox spi_monitor2scoreboard;

    function new(virtual uart_interface uart_vif, virtual spi_interface spi_vif);
        this.uart_vif = uart_vif;
        this.spi_vif = spi_vif;
        uart_monitor2scoreboard = new();
        spi_monitor2scoreboard = new();

        uart_agent0 = new(uart_vif, uart_monitor2scoreboard);
        spi_monitor0 = new(spi_vif, spi_monitor2scoreboard);
        scoreboard0 = new(uart_monitor2scoreboard,spi_monitor2scoreboard);
    endfunction

    task main();
        $display("Environment started time: %0t", $time);
        fork
            uart_agent0.main();
            spi_monitor0.main();
            scoreboard0.main();
        join
        $display("Environment ended time: %0t", $time);
    endtask

endclass //environment