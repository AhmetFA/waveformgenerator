class scoreboard;
    mailbox uart_monitor2scoreboard;
    mailbox spi_monitor2scoreboard;

    byte sent_byte=0;
    bit wait_version_report=0;
    bit wait_baud_rate_report=0;
    
    int receive_counter= 0;
    logic [7:0] uart_receive_char;
    logic [7:0] version_report[$];
    logic [7:0] baud_rate_report [$];
    logic [7:0] version_report_ref [0:11] = {8'h45,8'h4c,8'h45,8'h43,8'h54,8'h52,8'h41,8'h49,8'h43,8'h5f,8'h56,8'h31};
    logic [7:0] baud_rate_report_ref [4:0][7:0] = '{
    0:{8'h39,8'h36,8'h30,8'h30,8'h00,8'h00,8'h0a,8'h0d},
    1:{8'h31,8'h39,8'h32,8'h30,8'h30,8'h00,8'h0a,8'h0d},
    2:{8'h33,8'h38,8'h34,8'h30,8'h30,8'h00,8'h0a,8'h0d},
    3:{8'h35,8'h37,8'h36,8'h30,8'h30,8'h00,8'h0a,8'h0d},
    4:{8'h31,8'h31,8'h35,8'h32,8'h30,8'h30,8'h0a,8'h0d}};

    logic [15:0] SINE_LUT [0:255]= '{
        16'h3080,16'h3083,16'h3086,16'h3089,16'h308c,16'h308f,16'h3092,16'h3095,16'h3098,16'h309b,16'h309e,16'h30a2,16'h30a5,16'h30a7,16'h30aa,16'h30ad,
        16'h30b0,16'h30b3,16'h30b6,16'h30b9,16'h30bc,16'h30be,16'h30c1,16'h30c4,16'h30c6,16'h30c9,16'h30cb,16'h30ce,16'h30d0,16'h30d3,16'h30d5,16'h30d7,
        16'h30da,16'h30dc,16'h30de,16'h30e0,16'h30e2,16'h30e4,16'h30e6,16'h30e8,16'h30ea,16'h30eb,16'h30ed,16'h30ee,16'h30f0,16'h30f1,16'h30f3,16'h30f4,
        16'h30f5,16'h30f6,16'h30f8,16'h30f9,16'h30fa,16'h30fa,16'h30fb,16'h30fc,16'h30fd,16'h30fd,16'h30fe,16'h30fe,16'h30fe,16'h30ff,16'h30ff,16'h30ff,
        16'h30ff,16'h30ff,16'h30ff,16'h30ff,16'h30fe,16'h30fe,16'h30fe,16'h30fd,16'h30fd,16'h30fc,16'h30fb,16'h30fa,16'h30fa,16'h30f9,16'h30f8,16'h30f6,
        16'h30f5,16'h30f4,16'h30f3,16'h30f1,16'h30f0,16'h30ee,16'h30ed,16'h30eb,16'h30ea,16'h30e8,16'h30e6,16'h30e4,16'h30e2,16'h30e0,16'h30de,16'h30dc,
        16'h30da,16'h30d7,16'h30d5,16'h30d3,16'h30d0,16'h30ce,16'h30cb,16'h30c9,16'h30c6,16'h30c4,16'h30c1,16'h30be,16'h30bc,16'h30b9,16'h30b6,16'h30b3,
        16'h30b0,16'h30ad,16'h30aa,16'h30a7,16'h30a5,16'h30a2,16'h309e,16'h309b,16'h3098,16'h3095,16'h3092,16'h308f,16'h308c,16'h3089,16'h3086,16'h3083,
        16'h3080,16'h307c,16'h3079,16'h3076,16'h3073,16'h3070,16'h306d,16'h306a,16'h3067,16'h3064,16'h3061,16'h305d,16'h305a,16'h3058,16'h3055,16'h3052,
        16'h304f,16'h304c,16'h3049,16'h3046,16'h3043,16'h3041,16'h303e,16'h303b,16'h3039,16'h3036,16'h3034,16'h3031,16'h302f,16'h302c,16'h302a,16'h3028,
        16'h3025,16'h3023,16'h3021,16'h301f,16'h301d,16'h301b,16'h3019,16'h3017,16'h3015,16'h3014,16'h3012,16'h3011,16'h300f,16'h300e,16'h300c,16'h300b,
        16'h300a,16'h3009,16'h3007,16'h3006,16'h3005,16'h3005,16'h3004,16'h3003,16'h3002,16'h3002,16'h3001,16'h3001,16'h3001,16'h3000,16'h3000,16'h3000,
        16'h3000,16'h3000,16'h3000,16'h3000,16'h3001,16'h3001,16'h3001,16'h3002,16'h3002,16'h3003,16'h3004,16'h3005,16'h3005,16'h3006,16'h3007,16'h3009,
        16'h300a,16'h300b,16'h300c,16'h300e,16'h300f,16'h3011,16'h3012,16'h3014,16'h3015,16'h3017,16'h3019,16'h301b,16'h301d,16'h301f,16'h3021,16'h3023,
        16'h3025,16'h3028,16'h302a,16'h302c,16'h302f,16'h3031,16'h3034,16'h3036,16'h3039,16'h303b,16'h303e,16'h3041,16'h3043,16'h3046,16'h3049,16'h304c,
        16'h304f,16'h3052,16'h3055,16'h3058,16'h305a,16'h305d,16'h3061,16'h3064,16'h3067,16'h306a,16'h306d,16'h3070,16'h3073,16'h3076,16'h3079,16'h307c
    };
    int sine_lut_addr=0;

    byte command_select=0;
    byte wave_select=0;
    byte frequency_select=0;
    byte baud_rate_select=0;
    
    uart_seq_item uart_seq_item0;
    spi_seq_item spi_seq_item0;

    covergroup serial_comm;
        coverpoint command_select { 
            bins command[2] = {8'h61,8'h62};
        }
        coverpoint wave_select{
            bins  wave[4] = {[16'h41:16'h44]};
        }
        coverpoint frequency_select{
            bins  frequency[10] = {[16'h30:16'h39]};
        }
        freq_wave_cross: cross wave_select, frequency_select;
        coverpoint uart_receive_char{
            bins version_rep = (8'h45=>8'h4c=>8'h45=>8'h43=>8'h54=>8'h52=>8'h41=>8'h49=>8'h43=>8'h5f=>8'h56=>8'h31);
        }
    endgroup


    function new(mailbox uart_monitor2scoreboard, mailbox spi_monitor2scoreboard);
        this.uart_monitor2scoreboard = uart_monitor2scoreboard;
        this.spi_monitor2scoreboard= spi_monitor2scoreboard;
        serial_comm = new;
    endfunction
    
    task main;
        $display("Scoreboard started time: %0t", $time);
        fork
            uart_check();
            //spi_check();
        join
        $display("Scoreboard ended time: %0t", $time);
    endtask
    
    task uart_check;
        forever begin
            uart_monitor2scoreboard.get(uart_seq_item0);
            $display("Transaction detected! %0t", $time);
            uart_seq_item0.display();
            if (uart_seq_item0.tx_captured == 1) begin
                //Coverage Collection for Test scenarios
                if ( (uart_seq_item0.tx_data inside {[8'h41:8'h44]}) && !(sent_byte inside {[16'h41:16'h44]}) ) begin
                    sent_byte = uart_seq_item0.tx_data;
                end else if ( (sent_byte inside {[8'h41:8'h44]}) && (uart_seq_item0.tx_data inside {[16'h30:16'h39]}) ) begin
                    frequency_select = uart_seq_item0.tx_data;
                    wave_select = sent_byte;
                end else if (uart_seq_item0.tx_data == 8'h61) begin
                    wait_version_report = 1;
                    command_select = 8'h61;
                end else if (uart_seq_item0.tx_data == 8'h62) begin
                    command_select = 8'h62;
                    wait_baud_rate_report = 1;
                end
                serial_comm.sample();
                sent_byte = uart_seq_item0.tx_data;
            end
            
            if (uart_seq_item0.rx_captured == 1) begin
                $display("wait_version : %b",wait_version_report);
                $display("wait_baud : %b",wait_baud_rate_report);
                $display("receive_counter : %b",receive_counter);
                uart_receive_char = uart_seq_item0.rx_data;
                serial_comm.sample();
                //Checkers for responses
                if ( (wait_version_report == 1) && (receive_counter < 12)) begin
                    version_report.push_back(uart_seq_item0.rx_data);
                    receive_counter++;
                    if (receive_counter == 12) begin
                        foreach(version_report[i])
                            if (version_report[i] != version_report_ref[i])
                                $display("Error Malfuctioned version!!!");
                            else
                                $display("Successfull Version Report");
                        wait_version_report = 0;
                        receive_counter = 0;
                    end
                end else if ((wait_baud_rate_report == 1) && (receive_counter < 8)) begin
                    baud_rate_report.push_back(uart_seq_item0.rx_data);
                    receive_counter++;
                    if (receive_counter == 8) begin
                        foreach(baud_rate_report[i])
                            if (baud_rate_report[i] != baud_rate_report_ref[uart_seq_item0.sw][i])
                                $display("Error Malfuctioned Baudrate report!!!");
                            else
                                $display("Successfull Version Baudrate report");
                        wait_baud_rate_report = 0;
                        receive_counter = 0;
                    end
                end else
                    $display("Error Malfuctioned receive occurred!!!");
            end
        end
    endtask

    task spi_check;
        forever begin
            spi_monitor2scoreboard.get(spi_seq_item0);
            spi_seq_item0.disp();
            if  (wave_select == 8'h42) begin
                if (sine_lut_addr < 32) begin
                        assert ( (spi_seq_item0.spi_din == 16'h30FF) )
                            $display("Square wave check pass");
                        else
                            $error("Square wave check fail %h %d",spi_seq_item0.spi_din ,sine_lut_addr);
                end else begin 
                        assert ( (spi_seq_item0.spi_din == 16'h3000) )
                            $display("Square wave check pass");
                        else
                            $error("Square wave check fail %h %d",spi_seq_item0.spi_din ,sine_lut_addr);
                end
                sine_lut_addr++;
                sine_lut_addr = sine_lut_addr % 64;
            end else if (wave_select == 8'h44) begin
                assert ( (spi_seq_item0.spi_din == SINE_LUT[sine_lut_addr]) )
                    $display("Sine LUT check pass");
                else
                    $error("Sine LUT check fail %h %h",spi_seq_item0.spi_din ,SINE_LUT[sine_lut_addr]);

                sine_lut_addr++;
                sine_lut_addr = sine_lut_addr % 256;
            end

        end
    endtask
endclass
