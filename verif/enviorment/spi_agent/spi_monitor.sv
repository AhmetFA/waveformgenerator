`include "spi_seq_item.sv"
class spi_monitor;
    virtual spi_interface spi_vif;
    mailbox spi_monitor2scoreboard;
    spi_seq_item spi_seq_item0;

    function new(virtual spi_interface spi_vif, mailbox spi_monitor2scoreboard);
        this.spi_vif = spi_vif;
        this.spi_monitor2scoreboard = spi_monitor2scoreboard;
    endfunction

    task main();
        forever begin
            spi_seq_item0 = new();
            @(negedge spi_vif.sync_n);
            for(int i =0; i < 16 ;i++) begin   
                @(posedge spi_vif.sclk);
                spi_seq_item0.spi_din[15-i] = spi_vif.dac_data;
            end
            spi_monitor2scoreboard.put(spi_seq_item0);
        end
    endtask
endclass