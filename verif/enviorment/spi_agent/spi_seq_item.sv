class spi_seq_item;
    logic [15:0] spi_din;
    extern function void disp;
endclass

function void spi_seq_item::disp();
    $display("SPI captured data is : %h",spi_din);
endfunction