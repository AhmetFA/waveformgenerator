The frequencies of the waveforms can be programmed to the following frequencies over a UART interface. Limit of the counter in en_gen module will be changed with given commands.
•Default counter limit of en_gen module is 1000 and the default wave type for wave_gen_top is saw-tooth.
•The counter limit will be modified according to the following ASCII table. The frequency adjustment will be done from the keyboard.
Also, version and baudrate informations will be printed according to received command over UART.
Table 1: Frequency Selection
-ASCII-   -Decimal-    -Counter Limit-   -Frequency-
 0         48           1000              390 Hz
 1         49           900               434 Hz
 2         50           800               488 Hz
 3         51           700               558 Hz
 4         52           600               651 Hz
 5         53           500               781 Hz
 6         54           400               976 Hz
 7         55           300               1.3 kHz
 8         56           200               1.953 kHz
 9         57           100               3.9 kHz

Table 2: Waveform Selection
-ASCII-   -Decimal-     -Wave Type-
 A         48            Saw-tooth
 B         49            Square
 C         50            Triangle

Table 3: Information Write Selection
-ASCII-   -Decimal-    -Write Type-
 a         61           Version
 b         62           Baudrate
