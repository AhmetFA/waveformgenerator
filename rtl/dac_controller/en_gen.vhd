-- File Name : en_gen.vhd
-- Entity Name : en_gen
-- Architecture Name : behav
-- Input Ports
-- clk 100 MHz : Clock
-- cmd_rdy : Command ready signal from cmd_handler. If set to high, en_out frequency is started according to the freq input
-- freq(7:0) en_out frequency value from the UART interface
-- Output Ports
-- en_out Enable output connected to wave_gen and sync_data_gen blocks. Used to set the frequency of the waveform to be generated
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
entity en_gen is
  Port ( 
  -- Input Ports
  Clock             : in  Std_logic;
  reset				: in  Std_logic;
  cmd_ready			: in  Std_logic;
  freq				: in  Std_logic_vector(7 downto 0);
  -- Output Ports
  en_out			: out std_logic

  );
end en_gen;

architecture Behavioral of en_gen is

signal count_limit : integer range 0 to 1023; 
signal counter	   : integer range 0 to 1023;

begin

process(Clock,reset,cmd_ready,freq)
begin
if rising_edge(Clock) then
	if reset = '1' then
		count_limit <= 1000;
		counter     <= 0;
	else
	
		if cmd_ready = '1' then
			case freq is 
			when x"30"  => count_limit <= 1000;
			when x"31"  => count_limit <= 0900;
			when x"32"  => count_limit <= 0800;
			when x"33"  => count_limit <= 0700;
			when x"34"  => count_limit <= 0600;
			when x"35"  => count_limit <= 0500;
			when x"36"  => count_limit <= 0400;
			when x"37"  => count_limit <= 0300;
			when x"38"  => count_limit <= 0200;
			when x"39"  => count_limit <= 0100;
			when others => count_limit <= 1000;
			end case;
		
			counter <= 0;--reset the counter when new frequency is given
		else
			count_limit <= count_limit;
		end if;
	
		if counter < count_limit then
			counter <= counter +1;
			en_out <= '0';
		elsif count_limit = count_limit then
			counter <=  0 ; 
			en_out  <= '1';
		else
			counter <=  0 ; 
			en_out <=  '0';		
		end if;
	
   end if;
end if;
end process;
end Behavioral;