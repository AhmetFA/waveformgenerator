-- File Name sync_data_gen.vhd
-- Entity Name sync_data_gen
-- Architecture Name behav
-- Input Ports
-- clk 100 MHz Clock
-- en Enable signal from en_gen module. Used to set the frequency of the waveform
-- edge_low Needed to meet specifications of PMOD DAC
-- wave(7:0) One byte waveform data from the wave_gen module
-- Output Ports
-- sync Sync output to be connected to external PMOD DA1 module
-- dac_data Data output to be connected to external PMOD DA1 module
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
entity sync_data_gen is
  Port ( 
	Clock 	 : in  std_logic;
	reset 	 : in  std_logic;
	Enable   : in  std_logic;
	edge_low : in  std_logic;
	wave  	 : in  std_logic_vector(7 downto 0);
	sync	 : out std_logic;
	dac_data : out std_logic
  );
end sync_data_gen;

architecture Behavioral of sync_data_gen is
constant dac_config_byte : std_logic_vector(7 downto 0) := "00110000";
signal counter : integer range 0 to 15;
signal wave_buffer : std_logic_vector(15 downto 0);
type   States is (idle,send_data);
signal dac_drive_state : States;
begin

wave_buffer <= dac_config_byte & wave;

process(Clock,reset)
begin
if rising_edge(Clock) then
	if reset = '1' then
		sync 	 <= '1';
		dac_data <= '0';
		counter  <= 0;
		dac_drive_state<=idle;
	else
	   if edge_low = '1' then
	       dac_data <= wave_buffer(15-counter);
 		     case dac_drive_state is
 		     when idle =>
 		         sync 	 <= '1';
 		         dac_data <= '0';
   	             counter  <= 0;
   	             if Enable = '1' then
   	                dac_drive_state <= send_data;
   	                --sync 	 <= '0';
   	             else
   	                dac_drive_state <= idle;
   	             end if;   
 		     when send_data => 
                 if counter < 15 then
                    counter <= counter+1;
                    dac_drive_state <= send_data;
   	                sync 	 <= '0';
                 else
                 --sync 	 <= '1';
                    counter  <=  0 ;
                    dac_drive_state <= idle; 
                 end if;   
             when others =>
                 sync 	 <= '1';
                 dac_drive_state <= idle; 
             end case;
        else
            dac_drive_state<=dac_drive_state;
            counter  <= counter;
        end if;     
    end if;
end if;
end process;

end Behavioral;
