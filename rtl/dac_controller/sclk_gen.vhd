-- File Name sclk_gen.vhd
-- Entity Name sclk_gen
-- Architecture Name behav
-- Input Ports
-- clk 100 MHz Clock
-- Output Ports
-- sclk 25 MHz clock output. Clk divided by 4
-- edge_low Connected to sync_data_gen block. Needed to meet specifications of PMOD DAC
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
entity sclk_gen is
  Port ( 
	Clock 	 : in  std_logic;
	reset 	 : in  std_logic;
	sclk  	 : out std_logic;
	edge_low : out std_logic
  );
end sclk_gen;

architecture Behavioral of sclk_gen is
signal counter 		: std_logic_vector(1 downto 0);
signal edge_buff	: std_logic_vector(1 downto 0);
begin

process(Clock,reset)
begin
if rising_edge(Clock) then
edge_buff <= counter;
	if reset = '1' then
	counter <= "00";
	else
		case counter is
		when "00"   => counter <= "01";
		when "01"   => counter <= "10";
		when "10"   => counter <= "11";
		when "11"   => counter <= "00";
		when others => counter <= "00";
		end case;
    end if;
end if;
end process;

edge_low  <= edge_buff(1) and not edge_buff(0) and counter(1) and counter(0);
sclk      <= counter(1);

end Behavioral;