--Entity name 		: Baudrate_gen
--Architecture Name : behav
--Generics
--	  osc_freq     : Frequency (100MHz) := 100_000_000
--	  #_of_samples : Number of samples duringone baud
--Inputs
--    sw(2:0)
--	  rx_active
--	  tx_active
--Output Port
--	  baud_en_rx
--	  baud_en_tx
library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity Baud_gen is
  generic(
  osc_freq       : integer := 100_000_000;
  num_of_samples : integer := 16
  );
  port (
  Clock         :    in  Std_logic;
  reset			:    in  Std_logic;
  rx_active	    :	 in  Std_logic;
  tx_active     :    in  Std_logic;
  sw            :    in  Std_logic_vector(2 downto 0);
  baud_en_rx  	:    out Std_logic;
  baud_en_tx    :    out std_logic
  );
end;

architecture RTL of Baud_gen is

signal counter      : integer range 0 to 1023 ;--only first 10 needed as slowest baud is 9600(651 clock cycles)
signal baud_number  : integer range 0 to 1023 ;--only first 10 needed as slowest baud is 9600(651 clock cycles);
signal flag			: std_logic;
begin

process(clock,rx_active,tx_active)
begin

baud_switch : case sw is
    when "000"  => baud_number <= integer(real(osc_freq/1_000_000)*6.51);--9600   baud with 16 pulses
    when "001"  => baud_number <= integer(real(osc_freq/1_000_000)*3.25);--19200  baud with 16 pulses
    when "010"  => baud_number <= integer(real(osc_freq/1_000_000)*1.63);--38400  baud with 16 pulses
    when "011"  => baud_number <= integer(real(osc_freq/1_000_000)*1.09);--57600  baud with 16 pulses
    when "100"  => baud_number <= integer(real(osc_freq/1_000_000)*0.54);--115200 baud with 16 pulses
    when others => baud_number <= integer(real(osc_freq/1_000_000)*6.51);--9600   baud for everything else
end case; 

if rising_edge(clock) then
    if reset = '1' then
    counter    <=  0 ;
    baud_en_rx <= '0';
    baud_en_tx <= '0';
    else
        if counter < baud_number then
        --we count till the baud number and when counter reaches last value 
        --flag goes '1' and otherwise it's '0'
        counter <= counter + 1;
        flag <= '0';
        else
        counter <= 0;
        flag <= '1';
        end if;
    
        if rx_active = '1' then 
        baud_en_rx <= flag;
        else
        baud_en_rx <= '0';
        end if;
    
        if tx_active = '1' then
        baud_en_tx <= flag;
        else
        baud_en_tx <= '0';
        end if;
        
    end if;
end if;
end process;


end;