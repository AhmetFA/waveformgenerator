----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.All;
use STD.textio.all;
use ieee.std_logic_textio.all;

library work;
use work.wave_gen_pgk.all;


entity wave_gen_top_tb is
--  Port ( );
end wave_gen_top_tb;

architecture Behavioral of wave_gen_top_tb is
component wave_gen_top is
 Generic(
  osc_freq      : integer := 100_000_000; 
  Data_width 	: integer := 8;
  no_of_sample  : integer := 16
 );
 Port ( 
 --inputs
 Clock		: in std_logic;
 reset 	    : in  std_logic;
 uart_din	: in std_logic;
 sw			: in std_logic_vector(2 downto 0);
 --outputs
 uart_dout	: out std_logic;
 sync_n		: out std_logic;
 dac_data 	: out std_logic;
 sclk		: out std_logic;
 led		: out std_logic_vector(7 downto 0)
 );
end component wave_gen_top;
component Dac_Model is
  Port ( 
  reset     : in std_logic;
  sync_n	: in std_logic;
  dac_data  : in std_logic;
  sclk      : in std_logic;
  dac_out   : out real
  );
end component Dac_Model;

 signal baud_time : time:=105us;--time spend to send one bit by uart in us
----------------------------------
 --inputs
 signal  Clock		:  std_logic;
 signal  reset 	    :  std_logic:= '1';
 signal  uart_din	:  std_logic;
 signal  sw			:  std_logic_vector(2 downto 0):="000";
 --outputs
 signal  uart_dout	: std_logic;
 signal  sync_n		: std_logic;
 signal  dac_data 	: std_logic;
 signal  sclk		: std_logic;
 signal  led		: std_logic_vector(7 downto 0);
 ----------------------------------
 signal  rx_data_ready      : std_logic;
 signal  rx_data            : std_logic_vector(7 downto 0);
 signal  tx_send            : std_logic;
 signal  tx_active          : std_logic;
 
 signal  u_test_send : std_logic_vector(7 downto 0);
 
 type baud_rate_table is array (0 to 7 ) of std_logic_vector(7 downto 0);
 type version_table   is array (0 to 11) of std_logic_vector(7 downto 0);
 
 signal sw0  : baud_rate_table  :=  (x"39",x"36",x"30",x"30",x"00",x"00",x"0a",x"0d");
 signal sw1  : baud_rate_table  :=  (x"31",x"39",x"32",x"30",x"30",x"00",x"0a",x"0d");
 signal sw2  : baud_rate_table  :=  (x"33",x"38",x"34",x"30",x"30",x"00",x"0a",x"0d");
 signal sw3  : baud_rate_table  :=  (x"35",x"37",x"36",x"30",x"30",x"00",x"0a",x"0d");
 signal sw4  : baud_rate_table  :=  (x"31",x"31",x"35",x"32",x"30",x"30",x"0a",x"0d");
 signal switch : baud_rate_table;
 signal version : version_table :=  (x"45",x"4c",x"45",x"43",x"54",x"52",x"41",x"49",x"43",x"5f",x"56",x"31");
 
 signal baud_rate_readback : baud_rate_table:=(x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00");
 signal version_readback : version_table:=(x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00"); 

 signal counter      : integer := 0;
 signal baud_counter : integer := 0;
 signal num_of_error : integer:=0;
 
 signal test_mark    : std_logic;

 signal wave_sel           :  integer := 0;
 signal wave_freq          :  integer := 0;
 signal data_t : std_logic_vector(7 downto 0);
 signal data_f : std_logic_vector(7 downto 0);
 signal  dac_out    : real;
 
begin
dut0: wave_gen_top
 Generic Map(
  osc_freq      => 100_000_000,
  Data_width 	=> 8,
  no_of_sample  => 16
 )
 Port Map( 
 --inputs
 Clock		=>Clock,
 reset 	    =>reset,
 uart_din	=>uart_din,
 sw			=>sw,
 --outputs         
 uart_dout	=>uart_dout,
 sync_n		=>sync_n,
 dac_data 	=>dac_data,
 sclk		=>sclk,
 led		=>led
 );
dut1: uart_top
   generic map(
   osc_freq      =>100_000_000, 
   Data_width    => 8,
   no_of_sample  =>16
   )
   Port map ( 
   Clock		=>Clock,
   reset        =>reset,
   sw			=>sw, 
   rx_din       =>uart_dout,
   tx_data      =>u_test_send,
   tx_send      =>tx_send,
   tx_dout      =>uart_din,
   tx_active    =>tx_active,
   rx_data_ready=>rx_data_ready,
   rx_data      =>rx_data
   );
dut2: Dac_Model 
     Port Map( 
     reset     =>reset,    
     sync_n    =>sync_n,   
     dac_data  =>dac_data, 
     sclk      =>sclk,     
     dac_out   =>dac_out  
     );   
 baud_sel : process(sw)
             begin
             case sw is 
             when "000"  =>
                 switch    <= sw0;
                 baud_time <= 105us;
             when "001"  =>
                 switch <= sw1;
                 baud_time <= 52.1us;
             when "010"  =>
                 switch <= sw2;
                 baud_time <= 26.5us;
             when "011"  =>
                 switch <= sw3;
                 baud_time <= 17.4us;
             when "100"  =>
                 switch <= sw4;
                 baud_time <= 8.7us;
             when others =>
                 switch <= sw0;
                 baud_time <= 105us;
             end case;
 end process;
 
 clock_proc : process
              begin
              
              Clock <= '0';
              wait for 5ns;
              Clock <= '1';
              wait for 5ns;
 end process;
 
 
 
   data_test         : process
   variable     test_data: std_logic_vector(7 downto 0):="00000000";
       file             F: TEXT open READ_MODE is "counter.txt";
   variable             L: LINE;
                        begin
                        reset <= '1'; 
                        wait for 10ns;
                        tx_send <= '0';
                        reset <= '0';
                        wait for baud_time;
                       
							for counter in 0 to 11 loop
								readline(F,L);
								read(L,test_data);
    
								if counter mod 2 = 0 then
									sw <= std_logic_vector(to_unsigned(counter/2,3)); 
								else                           
									sw <= sw;
								end if;
        
							u_test_send <= test_data;
							tx_send <= '1';
							wait for 10ns;
							tx_send <= '0';
							wait for 15*10*baud_time;
							end loop;

-----------------------------------------------------------
						u_test_send<="00000000";
						sw <= "000";
						reset <= '1'; 
						wait for 10ns;
						tx_send <= '0';
						reset <= '0';
						data_t <= x"41";
						data_f <= x"30";
						wait for baud_time;
						
						for counter in 0 to 5 loop
							for  wave_sel  in 0 to 3 loop
								for wave_freq in 0 to 9 loop                               
									u_test_send <= data_t;
									wait for 10ns;            
									tx_send     <= '1';
									wait for 10ns;
									tx_send <= '0';
									wait for 15*baud_time;
                                    ------------            
									u_test_send <= data_f; 
									wait for 10ns;            
									tx_send     <= '1';
									wait for 10ns;
									tx_send <= '0';
									wait for 15*baud_time; 
									wait for 5ms;
									data_f <= std_logic_vector(unsigned(data_f) + "00000001");                                                        
								end loop;
									data_f <= x"30";
									data_t <= std_logic_vector(unsigned(data_t) + "00000001");   
							end loop;
						data_t<= x"41";
						sw    <=std_logic_vector(unsigned(sw) + "001");
						end loop;
						wait;            
						end process;               

receive_check : process(rx_data_ready,clock,u_test_send,rx_data)
                begin
                if rising_edge(clock) then
                   if  u_test_send = x"61" then
                       if counter < 12 then
                            if rx_data_ready = '1' then
                                counter <= counter + 1;
                                version_readback(counter)<=rx_data;
                            end if;                             
                       elsif counter = 12 then
                            counter <= 0;
                            if version_readback = version then
                                version_readback<=(x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00");
                                num_of_error <= num_of_error;
                             else
                                version_readback<=(x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00");
                                num_of_error <= num_of_error +1;
                             end if;
                       end if; 

                                                           
                     elsif  u_test_send = x"62" then
                        if baud_counter < 8 then
                            if rx_data_ready = '1' then
                               baud_counter <= baud_counter + 1;
                               baud_rate_readback(baud_counter)<=rx_data;
                            end if; 
                        else
                            baud_counter <= 0;
                            baud_rate_readback <= (x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00");
                                if baud_rate_readback = switch then
                                    num_of_error <= num_of_error;
                                else
                                    num_of_error <= num_of_error +1;
                                end if;
                        end if;                           
                    end if; 
                end if;                     
                end process; 
                
              test_mark_proc:process(num_of_error)
                             begin
                             if num_of_error = 0 then
                             test_mark <= '1';---passed
                             else
                             test_mark <= '0';
                             end if;
                             end process;
------------------------------------------------------
	
							 
end Behavioral;
