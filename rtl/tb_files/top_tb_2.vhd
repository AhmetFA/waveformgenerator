----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.All;
use STD.textio.all;
use ieee.std_logic_textio.all;

library work;
use work.wave_gen_pgk.all;


entity top_tb_2 is
--  Port ( );
end top_tb_2;

architecture Behavioral of top_tb_2 is
component wave_gen_top is
 Generic(
  osc_freq      : integer := 100_000_000; 
  Data_width 	: integer := 8;
  no_of_sample  : integer := 16
 );
 Port ( 
 --inputs
 Clock		: in std_logic;
 reset 	    : in  std_logic;
 uart_din	: in std_logic;
 sw			: in std_logic_vector(2 downto 0);
 --outputs
 uart_dout	: out std_logic;
 sync_n		: out std_logic;
 dac_data 	: out std_logic;
 sclk		: out std_logic;
 led		: out std_logic_vector(7 downto 0)
 );
end component wave_gen_top;
component Dac_Model is
  Port ( 
  reset     : in std_logic;
  sync_n	: in std_logic;
  dac_data  : in std_logic;
  sclk      : in std_logic;
  dac_out   : out real
  );
end component Dac_Model;
 signal baud_time : time:=105us;--time spend to send one bit by uart in us
----------------------------------
 --inputs
 signal  Clock		:  std_logic;
 signal  reset 	    :  std_logic:= '1';
 signal  uart_din	:  std_logic;
 signal  sw			:  std_logic_vector(2 downto 0):="000";
 --outputs
 signal  uart_dout	: std_logic;
 signal  sync_n		: std_logic;
 signal  dac_data 	: std_logic;
 signal  sclk		: std_logic;
 signal  led		: std_logic_vector(7 downto 0);
 signal  dac_out    : real;
 ----------------------------------
 signal  rx_data_ready      : std_logic;
 signal  rx_data            : std_logic_vector(7 downto 0);
 signal  tx_send            : std_logic;
 signal  tx_active          : std_logic;
 
 signal  u_test_send : std_logic_vector(7 downto 0);
 
 signal wave_sel           :  integer := 0;
 signal wave_freq          :  integer := 0;
 signal data_t : std_logic_vector(7 downto 0);
 signal data_f : std_logic_vector(7 downto 0);
 
begin
dut0: wave_gen_top
 Generic Map(
  osc_freq      => 100_000_000,
  Data_width 	=> 8,
  no_of_sample  => 16
 )
 Port Map( 
 --inputs
 Clock		=>Clock,
 reset 	    =>reset,
 uart_din	=>uart_din,
 sw			=>sw,
 --outputs         
 uart_dout	=>uart_dout,
 sync_n		=>sync_n,
 dac_data 	=>dac_data,
 sclk		=>sclk,
 led		=>led
 );
dut1: uart_top
   generic map(
   osc_freq      =>100_000_000, 
   Data_width    => 8,
   no_of_sample  =>16
   )
   Port map ( 
   Clock		=>Clock,
   reset        =>reset,
   sw			=>sw, 
   rx_din       =>uart_dout,
   tx_data      =>u_test_send,
   tx_send      =>tx_send,
   tx_dout      =>uart_din,
   tx_active    =>tx_active,
   rx_data_ready=>rx_data_ready,
   rx_data      =>rx_data
   );
dut2: Dac_Model 
     Port Map( 
     reset     =>reset,    
     sync_n    =>sync_n,   
     dac_data  =>dac_data, 
     sclk      =>sclk,     
     dac_out   =>dac_out  
     );
 baud_sel : process(sw)
             begin
             case sw is 
             when "000"  =>
                 baud_time <= 105us;
             when "001"  =>
                 baud_time <= 52.1us;
             when "010"  =>
                 baud_time <= 26.5us;
             when "011"  =>
                 baud_time <= 17.4us;
             when "100"  =>
                 baud_time <= 8.7us;
             when others =>
                 baud_time <= 105us;
             end case;
 end process;
 
 clock_proc : process
              begin
              
              Clock <= '0';
              wait for 5ns;
              Clock <= '1';
              wait for 5ns;
 end process;
 
   data_test         : process
                       begin
                       u_test_send<="00000000";
                       sw <= "100";
                       reset <= '1'; 
                       wait for 10ns;
                       tx_send <= '0';
                       reset <= '0';
                       data_t <= x"41";
                       data_f <= x"30";
                       wait for baud_time;
for  wave_sel  in 0 to 3 loop
                            
    for wave_freq in 0 to 9 loop                               
            u_test_send <= data_t;
            wait for 10ns;            
            tx_send     <= '1';
            wait for 10ns;
            tx_send <= '0';
            wait for 15*baud_time;
------------------------------------------------            
            u_test_send <= data_f; 
            wait for 10ns;            
            tx_send     <= '1';
            wait for 10ns;
            tx_send <= '0';
            wait for 15*baud_time; 
    wait for 5ms;
    data_f <= std_logic_vector(unsigned(data_f) + "00000001");                                                        
    end loop;
    data_f <= x"30";
    data_t <= std_logic_vector(unsigned(data_t) + "00000001");   
end loop;        
            wait;            
            end process;               

end Behavioral;
