----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use STD.textio.all;
use ieee.std_logic_textio.all;

entity Dac_Model is
  Port ( 
  reset     : in std_logic;
  sync_n	: in std_logic;
  dac_data  : in std_logic;
  sclk      : in std_logic;
  dac_out   : out real
  );
end Dac_Model;

architecture Behavioral of Dac_Model is
type   States is (idle,receive);
signal dac_state : States;
signal spi_read : std_logic_vector(15 downto 0):="0000000000000000";
signal counter  : integer:= 0;
begin

dac_receive_proc :process(reset,sync_n,dac_data,sclk)
       file          F: TEXT open write_MODE is "waveout.txt";
   variable          L: LINE;
begin

    if reset = '1' then
        dac_state <= idle;
        dac_out   <= 0.0;
        counter   <= 0;
    else
        case dac_state is 
        when    idle =>
        counter <= 0;
            if falling_edge(sync_n) then
                dac_state <= receive;
            else
                dac_state <= idle   ;
            end if;
        when receive =>
            if sync_n = '0' then
                if counter < 16 then
                    if rising_edge(sclk) then
                        spi_read(15 - counter) <= dac_data;
                        counter <= counter + 1;
                    end if;     
                end if;
            else
                dac_state <= idle;
                dac_out <= real(to_integer(unsigned(spi_read(7 downto 0))))*3.3/256.0; 
                write(L,real(to_integer(unsigned(spi_read(7 downto 0))))*3.3/256.0);
                writeline(F,L);
            end if;            
        when others  =>
        end case;
    end if;


end process;
end Behavioral;
