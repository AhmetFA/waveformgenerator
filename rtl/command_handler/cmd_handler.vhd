-- File Name cmd_handler.vhd
-- Entity Name cmd_handler
-- Architecture Name behav
-- Input Ports
-- clk 100 MHz Clock
-- rx_data_ready : UART RX data is ready to be used by cmd_handler
-- rx_data(7:0)  : UART RX one byte data
-- tx_active     : UART is sending TX data. This signal is active high when UART is transmitting
-- sw(2:0) 		 : 3 bits switch bus connected to the switches on the board
-- Output Ports
-- cmd_ready 	 : Command received from the UART interface is ready to be used by other blocks
-- cmd(15:0) 	 : 16 bits command received from the UART interface
-- tx_send 	  	 : When active high, informs u_tx module to sample the data to be sent over the UART interface
-- tx_data(7:0) One byte TX data to be sent over the UART interface. Connect to u_tx module
--
-- VERSION is 45h, 4ch, 45h, 43h, 54h, 52h, 41h, 49h, 43h, 5fh, 56h, 31h
-- * BAUDRATE is 39h, 36h, 30h, 30h, 00h, 00h, 0ah, 0dh in order if sw(2:0) equals 000b or others
-- * BAUDRATE is 31h, 39h, 32h, 30h, 30h, 00h, 0ah, 0dh in order if sw(2:0) equals 001b
-- * BAUDRATE is 33h, 38h, 34h, 30h, 30h, 00h, 0ah, 0dh in order if sw(2:0) equals 010b
-- * BAUDRATE is 35h, 37h, 36h, 30h, 30h, 00h, 0ah, 0dh in order if sw(2:0) equals 011b
-- * BAUDRATE is 31h, 31h, 35h, 32h, 30h, 30h, 0ah, 0dh in order if sw(2:0) equals 100b
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity cmd_handler is
  Generic(
  osc_freq      : integer := 100_000_000; 
  Data_width 	: integer := 8;
  no_of_sample  : integer := 16
  );
  Port ( 
  -- Input Ports
  Clock             : in  Std_logic;
  reset				: in  Std_logic;
  rx_data_ready 	: in  Std_logic; 
  rx_data           : in  Std_logic_vector(7 downto 0);
  tx_active			: in  Std_logic;
  sw				: in  Std_logic_vector(2 downto 0);
-- Output Ports
  cmd_ready			: out  Std_logic;
  cmd				: out  Std_logic_vector(15 downto 0);
  tx_send			: out  Std_logic;
  tx_data			: out  Std_logic_vector(7 downto 0)
  );
end cmd_handler;

architecture Behavioral of cmd_handler is

type baud_rate_table is array (0 to 7) of std_logic_vector(7 downto 0);
signal sw0  : baud_rate_table;--9600 and others
signal sw1  : baud_rate_table;--19200
signal sw2  : baud_rate_table;--38400
signal sw3  : baud_rate_table;--57600
signal sw4  : baud_rate_table;--115200
signal baud : baud_rate_table;
signal tx_send1 : Std_logic;
type version_table is array (0 to 11) of std_logic_vector(7 downto 0);
signal version : version_table;

type states is (idle,write_version,write_baudrate,dac_config);
signal command_state : states;

signal flag : std_logic;
signal counter : integer range 0 to 15;
signal rx_data_buff   : Std_logic_vector(7 downto 0);
begin
--

sw0 	<= (x"39",x"36",x"30",x"30",x"00",x"00",x"0a",x"0d");
sw1 	<= (x"31",x"39",x"32",x"30",x"30",x"00",x"0a",x"0d");
sw2 	<= (x"33",x"38",x"34",x"30",x"30",x"00",x"0a",x"0d");
sw3 	<= (x"35",x"37",x"36",x"30",x"30",x"00",x"0a",x"0d");
sw4 	<= (x"31",x"31",x"35",x"32",x"30",x"30",x"0a",x"0d");
version <= (x"45",x"4c",x"45",x"43",x"54",x"52",x"41",x"49",x"43",x"5f",x"56",x"31");
tx_send <= tx_send1;
process(Clock,tx_active,rx_data_ready,flag,counter)
begin
		if sw = "000" then
		baud    <= sw0; 
		elsif sw = "001" then
		baud    <= sw1; 
		elsif sw = "010" then
		baud    <= sw2; 
		elsif sw = "011" then
		baud    <= sw3; 
		elsif sw = "100" then
		baud    <= sw4; 
		else
		baud    <= sw0; 
		end if;

if rising_edge(Clock) then
	if reset = '1' then
	command_state <= idle;  
	else		
		case command_state is 
		when idle			=>
		cmd_ready <= '0';
		counter <=  0 ;
		tx_send1 <= '0';	
		flag<= '0';
		tx_data <= (others => '0');
		cmd     <= (others => '0');
			if rx_data_ready = '1' then
				if    rx_data = x"61" then
				command_state <= write_version;
				elsif rx_data = x"62" then
				command_state <= write_baudrate;
				elsif rx_data > x"40" and rx_data < x"45" then
				command_state <= dac_config;
				cmd(15 downto 8)<=rx_data;
				end if;
			else
				command_state  <= idle;
			end if;
	--------------------------------------------------------
		when write_version  =>
		if counter < 12 then
			if tx_active = '0' and tx_send1= '0' then
				tx_data <= version(counter);
				counter <= counter + 1;
				tx_send1 <= '1';
			else
				tx_send1 <= '0';	
			end if;
		elsif counter = 12 and tx_active = '0' then
			if  flag = '0' then
				flag <= '1';
				tx_send1 <= '0';
			else   
				flag <= '0'; 
				counter 	     <=  0 ;
				command_state  <= idle;
			end if;	
		else
			command_state <= write_version;	      	  
		end if;	
	--------------------------------------------------------
	when write_baudrate =>
		if counter < 8 then
			if tx_active = '0' and tx_send1= '0' then
				tx_data <= baud(counter);
				counter <= counter + 1;
				tx_send1 <= '1';
			else
				tx_send1 <= '0';    			
			end if;
		elsif counter = 8 and tx_active = '0' then
			if  flag = '0' then
				flag <= '1';
            tx_send1 <= '0';
			else   
				flag <= '0'; 
				counter          <=  0 ;
				command_state  <= idle;
			end if;
		else
		  command_state <= write_baudrate;	     
		end if;		
	--------------------------------------------------------
		when dac_config 	=> 
		if rx_data_ready = '1' then
			if rx_data > x"2F" and rx_data < x"3A" then
			cmd(7 downto 0)<=rx_data;
			cmd_ready <= '1';
			command_state<=idle;
			else
			command_state<=idle;			
			end if;
		else
		    command_state <= dac_config;	
		end if;
	--------------------------------------------------------
		when others			=> command_state<= idle;
		end case;
   end if;
end if;

end process;
end Behavioral;