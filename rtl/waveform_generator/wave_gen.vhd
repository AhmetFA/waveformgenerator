-- File Name		 : wave_gen.vhd
-- Entity Name 		 : wave_gen
-- Architecture Name : behav
-- Input Ports
-- clk 				 : 100 MHz Clock
-- en 				 : Enable signal from en_gen module. Used to set the frequency of the waveform
-- cmd_rdy 		     : Command ready signal from cmd_handler. If set to high, waveform type (wave_type) is sampled to an internal register
-- wave_type(7:0) 	 : Waveform type value from the UART interface
-- Output Ports
-- wave_out(7:0) 	 : One byte waveform output
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
entity wave_gen is
  Port ( 
  -- Input Ports
  Clock             : in  Std_logic;
  reset				: in  Std_logic;
  cmd_ready			: in  Std_logic;
  enable			: in  std_logic;
  wave_type			: in  std_logic_vector(7 downto 0);
  wave_out			: out std_logic_vector(7 downto 0)

  );
end wave_gen;

architecture Behavioral of wave_gen is
component sine_bram is
port (
    -- Port A
    a_clk   : in  std_logic;
   -- a_wr    : in  std_logic;
    a_addr  : in  std_logic_vector(7 downto 0);
   -- a_din   : in  std_logic_vector(DATA-1 downto 0);
    a_dout  : out std_logic_vector(7 downto 0)
);
end component sine_bram;

constant count_limit : integer := 256; --sample period
signal   counter	 : integer range 0 to 1023;
signal   wave_format : std_logic_vector(1 downto 0);
signal   flag 		 : std_logic;
signal   sine 		 : std_logic_vector(7 downto 0);
signal   sine_addr   : std_logic_vector(7 downto 0);
begin

id0 : sine_bram port map (
    a_clk   => Clock,
    a_addr  => sine_addr,
    a_dout  => sine
);

process(Clock,reset,cmd_ready,enable,wave_type)
begin
if rising_edge(Clock) then
    if reset = '1' then
        wave_format <= "00";
        counter     <= 0;
        flag <= '0';
    elsif cmd_ready = '1' then 
            if     wave_type  = x"41" then 
                wave_format <= "00";--sawthooth
            elsif  wave_type  = x"42" then 
                wave_format <= "01";--square
               elsif  wave_type  = x"43" then 
                wave_format <= "10";--triangle		    
            elsif  wave_type  = x"44" then 
                wave_format <= "11";--sine
            else 
                wave_format <= "00";
            end if;
            
            flag <= '0';
            counter <= 0;--reset the counter when new frequency is given	
    else
        case wave_format is 
        when "00" => --sawthooth
            if	enable = '1' then
                if counter < count_limit-1 then
                    counter <= counter +1;
                elsif count_limit = count_limit then
                    counter <=  0 ;
                else
                    counter <= counter;
                end if;
            else
                counter <= counter;
            end if;
            wave_out <= std_logic_vector(to_unsigned(counter,8));
--------------------------------------------------------------------------			
        when "01" => --square
            if	enable = '1' then
                if 	  counter < count_limit/2-1 then
                    counter  <= counter +1;
                    wave_out <= x"FF";
                elsif counter >= count_limit/2-1 and counter < count_limit-1 then
                    counter <= counter +1;
                    wave_out <= x"00";
                elsif 	counter = count_limit-1 then
                    counter  <= 0;
                    wave_out <= x"FF";
                else
                    counter <= counter;						
                end if;
            else
                counter <= counter;
            end if;
--------------------------------------------------------------------------				
        when "10" => --triangle
            if	enable = '1' then
                if counter < count_limit*2-1 then
                    counter <= counter +2;
                else
                    counter <= 0;
                end if;
            else
                counter <= counter;
            end if;	
            
          if counter < count_limit then  
             wave_out <= std_logic_vector(to_unsigned(counter,8));	  
          elsif  counter > count_limit then
             wave_out <= std_logic_vector(to_unsigned(count_limit*2-counter,8));   
          else 
             wave_out <= x"ff";
          end if;         
--------------------------------------------------------------------------				
        when "11" => --sine
            if	enable = '1' then
                if counter < count_limit-1 then
                   counter <= counter +1;
                else 
                   counter <=  0 ;
                end if;
            else
                 counter <= counter;
            end if;			
            sine_addr <= std_logic_vector(to_unsigned(counter,8));
            wave_out <= sine;
--------------------------------------------------------------------------				
        when others =>
            if	enable = '1' then
                if counter < count_limit-1 then
                    counter <= counter +1;
                else
                    counter <=  0 ;
                end if;
            else
                counter <= counter;
            end if;
            wave_out <= std_logic_vector(to_unsigned(counter,8));			
        end case;	
    end if;
end if;
end process;


end Behavioral;