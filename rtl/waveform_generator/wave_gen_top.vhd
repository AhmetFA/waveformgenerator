----------------------------------------------------------------------------------
-- Company : 
-- Engineer: AFA
--File Name wave_gen_top.vhd
--Entity Name wave_gen_top
--Architecture Name struct
--Generics
--osc_freq Frequency integer constant (100_000_000)
--width UART width integer constant (8)
--no_of_sample Number of samples taken during one baud time integer constant (16)
--Input Ports
--clk 100 MHz Clock
--uart_din UART RX input
--sw(2:0) 3 bits switch bus connected to the switches on the board
--Output Ports
--uart_dout UART TX output
--sync DAC sync control
--dac_data DAC data
--s_clk DAC clock
--led(7:0) 8 bit led control bus
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.wave_gen_pgk.all;
---------------------------------------------
entity wave_gen_top is
 Generic(
  osc_freq      : integer := 100_000_000; 
  Data_width 	: integer := 8;
  no_of_sample  : integer := 16
 );
 Port ( 
 --inputs
 Clock		: in std_logic;
 reset 	    : in  std_logic;
 uart_din	: in std_logic;
 sw			: in std_logic_vector(2 downto 0);
 --outputs
 uart_dout	: out std_logic;
 sync_n		: out std_logic;
 dac_data 	: out std_logic;
 sclk		: out std_logic;
 led		: out std_logic_vector(7 downto 0)
 );
end wave_gen_top;

architecture Behavioral of wave_gen_top is
signal    tx_send			: std_logic;
signal    tx_data			: std_logic_vector(7 downto 0);

signal    tx_active			: std_logic; 
signal    rx_data_ready		: std_logic;
signal    rx_data	 		: std_logic_vector(7 downto 0);

signal    cmd_ready			: Std_logic;
signal    cmd				: Std_logic_vector(15 downto 0);

signal    en_out	     	: Std_logic;

signal    wave_out	 		: std_logic_vector(7 downto 0);

signal    edge_low	     	: Std_logic;

signal    reset1 	    	: std_logic;
signal    reset2 	    	: std_logic;

signal    sw1				: std_logic_vector(2 downto 0);
signal    sw2				: std_logic_vector(2 downto 0);

signal    uart_din1 	    	: std_logic;
signal    uart_din2 	    	: std_logic;
begin

process(Clock)
begin
--flip flops for asyncronous inputs
if rising_edge(clock) then
reset1<= reset ;
reset2<= reset1;

sw1   <= sw ;
sw2   <= sw1;

uart_din1 <= uart_din;
uart_din2 <= uart_din1;
end if;
end process;
----------------------------------------------------
id0 : uart_top
  generic map(
  osc_freq      =>  osc_freq,
  Data_width 	=>  Data_width,
  no_of_sample  =>  no_of_sample
  )
  Port Map( 
--  inputs
  Clock             =>Clock,
  reset	  			=>reset2,   		
  sw				=>sw2,			 
  rx_din			=>uart_din2,			
  tx_data			=>tx_data,			
  tx_send			=>tx_send,
-- outputs  
  tx_dout			=>uart_dout,			
  tx_active			=>tx_active,			
  rx_data_ready		=>rx_data_ready,
  rx_data			=>rx_data		
  );
  ------------------------------------------------------------------
 id1 :  cmd_handler
  Generic Map(
  osc_freq      => osc_freq,
  Data_width 	=> Data_width,
  no_of_sample  => no_of_sample
  )
  Port Map( 
  -- Input Ports
  Clock             => Clock,
  reset	  			=> reset2,   
  rx_data_ready 	=> rx_data_ready, 
  rx_data           => rx_data,
  tx_active			=> tx_active,	
  sw				=> sw2,
-- Output Ports
  cmd_ready			=> cmd_ready,
  cmd				=> cmd,
  tx_send			=> tx_send,
  tx_data			=> tx_data
  );
---------------------------------------------------
id2 : wave_gen
  Port Map( 
  -- Input Ports
  Clock             => Clock,
  reset	  			=> reset2,   
  cmd_ready			=> cmd_ready,
  enable			=> en_out,
  wave_type			=> cmd( 15 downto 8),
  wave_out			=> wave_out

  );
---------------------------------------------------
id3 : en_gen
  Port Map( 
  -- Input Ports
  Clock             => Clock,
  reset	  			=> reset2,   
  cmd_ready			=> cmd_ready,
  freq				=> cmd( 7 downto 0),
  -- Output Ports
  en_out			=> en_out

  );
---------------------------------------------------
id4 : sclk_gen
  Port map( 
  Clock             => Clock,
  reset	  			=> reset2,   
  sclk  	        => sclk,
  edge_low    		=> edge_low
  );
---------------------------------------------------
id5 : sync_data_gen
  Port Map( 
	Clock             => Clock,
	reset			  => reset2,   
    Enable 		      => en_out,
	edge_low 		  => edge_low,
	wave  	 		  => wave_out,
	sync	 		  => sync_n,
	dac_data 		  => dac_data
  );
---------------------------------------------------
led <= wave_out;
end Behavioral;
