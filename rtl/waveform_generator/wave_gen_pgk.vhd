----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Ahmet Fatih ANKARALI
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package wave_gen_pgk is
---------------------------------------------------
component uart_top is
  generic(
  osc_freq      : integer := 100_000_000; 
  Data_width 	: integer := 8;
  no_of_sample  : integer := 16
  );
  Port ( 
  Clock             : in  Std_logic;
  reset	     		: in  Std_logic;
  sw				: in  Std_logic_vector(2 downto 0); 
  rx_din			: in  std_logic;
  tx_data			: in  std_logic_vector(Data_width - 1 downto 0);
  tx_send			: in  std_logic;
  tx_dout			: out std_logic;
  tx_active			: out std_logic;
  rx_data_ready		: out std_logic;
  rx_data			: out std_logic_vector(Data_width - 1 downto 0)
  );
end component uart_top;
---------------------------------------------------
component cmd_handler is
  Generic(
  osc_freq      : integer := 100_000_000; 
  Data_width 	: integer := 8;
  no_of_sample  : integer := 16
  );
  Port ( 
  -- Input Ports
  Clock             : in  Std_logic;
  reset				: in  Std_logic;
  rx_data_ready 	: in  Std_logic; 
  rx_data           : in  Std_logic_vector(7 downto 0);
  tx_active			: in  Std_logic;
  sw				: in  Std_logic_vector(2 downto 0);
-- Output Ports
  cmd_ready			: out  Std_logic;
  cmd				: out  Std_logic_vector(15 downto 0);
  tx_send			: out  Std_logic;
  tx_data			: out  Std_logic_vector(7 downto 0)
  );
end component cmd_handler;
---------------------------------------------------
component wave_gen is
  Port ( 
  -- Input Ports
  Clock             : in  Std_logic;
  reset				: in  Std_logic;
  cmd_ready			: in  Std_logic;
  enable			: in  std_logic;
  wave_type			: in  std_logic_vector(7 downto 0);
  wave_out			: out std_logic_vector(7 downto 0)

  );
end component wave_gen;
---------------------------------------------------
component en_gen is
  Port ( 
  -- Input Ports
  Clock             : in  Std_logic;
  reset				: in  Std_logic;
  cmd_ready			: in  Std_logic;
  freq				: in  Std_logic_vector(7 downto 0);
  -- Output Ports
  en_out			: out std_logic

  );
end component en_gen;
---------------------------------------------------
component sclk_gen is
  Port ( 
	Clock 	 : in  std_logic;
	reset 	 : in  std_logic;
	sclk  	 : out std_logic;
	edge_low : out std_logic
  );
end component sclk_gen;
---------------------------------------------------
component sync_data_gen is
  Port 
( clock       : IN STD_LOGIC;
    reset     : IN STD_LOGIC;
    enable    : IN STD_LOGIC;
    edge_low  : IN STD_LOGIC;
    wave      : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
    sync      : OUT STD_LOGIC;
    dac_data  : OUT STD_LOGIC
  );
end component sync_data_gen;
---------------------------------------------------

end package wave_gen_pgk;

