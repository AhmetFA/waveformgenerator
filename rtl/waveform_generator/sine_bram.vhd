library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
 
entity sine_bram is
port (
    -- Port A
    a_clk   : in  std_logic;
   -- a_wr    : in  std_logic;
    a_addr  : in  std_logic_vector(7 downto 0);
   -- a_din   : in  std_logic_vector(DATA-1 downto 0);
    a_dout  : out std_logic_vector(7 downto 0)
);
end sine_bram;
 
architecture rtl of sine_bram is
    -- Shared memory
    type mem_type is array ( 255 downto 0 ) of std_logic_vector(7 downto 0);
    constant mem : mem_type:= 
    (x"80",x"83",x"86",x"89",x"8c",x"8f",x"92",x"95",x"98",x"9b",x"9e",x"a2",x"a5",x"a7",x"aa",x"ad",
     x"b0",x"b3",x"b6",x"b9",x"bc",x"be",x"c1",x"c4",x"c6",x"c9",x"cb",x"ce",x"d0",x"d3",x"d5",x"d7",
     x"da",x"dc",x"de",x"e0",x"e2",x"e4",x"e6",x"e8",x"ea",x"eb",x"ed",x"ee",x"f0",x"f1",x"f3",x"f4",
     x"f5",x"f6",x"f8",x"f9",x"fa",x"fa",x"fb",x"fc",x"fd",x"fd",x"fe",x"fe",x"fe",x"ff",x"ff",x"ff",
     x"ff",x"ff",x"ff",x"ff",x"fe",x"fe",x"fe",x"fd",x"fd",x"fc",x"fb",x"fa",x"fa",x"f9",x"f8",x"f6",
     x"f5",x"f4",x"f3",x"f1",x"f0",x"ee",x"ed",x"eb",x"ea",x"e8",x"e6",x"e4",x"e2",x"e0",x"de",x"dc",
     x"da",x"d7",x"d5",x"d3",x"d0",x"ce",x"cb",x"c9",x"c6",x"c4",x"c1",x"be",x"bc",x"b9",x"b6",x"b3",
     x"b0",x"ad",x"aa",x"a7",x"a5",x"a2",x"9e",x"9b",x"98",x"95",x"92",x"8f",x"8c",x"89",x"86",x"83",
     x"80",x"7c",x"79",x"76",x"73",x"70",x"6d",x"6a",x"67",x"64",x"61",x"5d",x"5a",x"58",x"55",x"52",
     x"4f",x"4c",x"49",x"46",x"43",x"41",x"3e",x"3b",x"39",x"36",x"34",x"31",x"2f",x"2c",x"2a",x"28",
     x"25",x"23",x"21",x"1f",x"1d",x"1b",x"19",x"17",x"15",x"14",x"12",x"11",x"0f",x"0e",x"0c",x"0b",
     x"0a",x"09",x"07",x"06",x"05",x"05",x"04",x"03",x"02",x"02",x"01",x"01",x"01",x"00",x"00",x"00",
     x"00",x"00",x"00",x"00",x"01",x"01",x"01",x"02",x"02",x"03",x"04",x"05",x"05",x"06",x"07",x"09",
     x"0a",x"0b",x"0c",x"0e",x"0f",x"11",x"12",x"14",x"15",x"17",x"19",x"1b",x"1d",x"1f",x"21",x"23",
     x"25",x"28",x"2a",x"2c",x"2f",x"31",x"34",x"36",x"39",x"3b",x"3e",x"41",x"43",x"46",x"49",x"4c",
     x"4f",x"52",x"55",x"58",x"5a",x"5d",x"61",x"64",x"67",x"6a",x"6d",x"70",x"73",x"76",x"79",x"7c");
begin
 
-- Port A
process(a_clk)
begin
    if rising_edge(a_clk) then
        a_dout <= mem(conv_integer(a_addr));
    end if;
end process;

end rtl;